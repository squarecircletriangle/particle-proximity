$(window).load(function(){
	StepOne.init();
});


var pusher = new Pusher('167c824c5f47f4ffe1a6', {
    authTransport: 'jsonp',
    authEndpoint: 'http://pusherauth.blocksauth.com/auth/authenticate.json'
 });

var channel = pusher.subscribe('private-particles');

channel.bind('pusher:subscription_succeeded', function() {
	console.log("OK Subscription is Good");
  	//var triggered = channel.trigger('client-someeventname', { your: data });
});

channel.bind('client-controller', function() {
	console.log("OK connected to client controller is Good");
});

function triggerVideowall(_action, _param){
	console.log("triggering: "+_action)
	var triggered = channel.trigger('client-controller', {
		action: _action,
		param: _param
	});
};

$(function(){
	//disable text selection
	$('body').on('selectstart',function(){ return false; });
});
//Global Timeout vars
var timerStepTwoB;
var timerStepTwoC;
var timerStepThree;
var timerStepFour;
var timerRestart;
//var globalTimeout;
var playerStatus = 0;

var SwitchContent = {
	fadeSpeed: 250,
	$transitionDiv: $("#transition"),
	switch:function(nextElement) {
		var _nextElement = nextElement;
		SwitchContent.$transitionDiv.fadeIn(SwitchContent.fadeSpeed, function(){
			$('.container.current').hide().removeClass('current');
			_nextElement.show().addClass('current');
			SwitchContent.$transitionDiv.fadeOut(SwitchContent.fadeSpeed);
		});
	}
};

var StepOne = {
	centerAdjust: 1,
	StepOneLength: $('.auto-fit-text').length - 1,
	init:function(){
		$('.auto-fit-text').css({ 'display' : 'inline-block' , 'font-size' : '12px' });
		StepOne.autoSizeText();
		StepOne.centerText();
	},
	autoSizeText:function(){
		$('.auto-fit-text').each(function(i){
			var _parent = $(this).parent();
			var _parentWidth = _parent.width();
			var _width = $(this).width();
			var _fontSize = Math.floor((_parentWidth/_width) * 12);
			$(this).css({ 'font-size' : _fontSize+'px' , 'display' : 'block' });			
		});
	},
	centerText:function(){
		$('.auto-fit-text').each(function(i){
			var _height = $(this).height();
			var _parent = $(this).parent();
			var _parentHeight = _parent.height();
			var _padding = Math.floor(((_parentHeight - _height) / 2) * StepOne.centerAdjust);
			$(this).css({ 'padding-top' : _padding+'px' , 'text-justify' : 'newspaper'});
			if( i == StepOne.StepOneLength ) {
				//Trigger First Video, then init gallery
				StepOne.textGallery();

				triggerVideowall('showThreeJS', 0);

				triggerVideowall('showModel', 0);
			}
		});
	},
	textGallery:function(){
		$("#step_one_gallery").css('visibility', 'visible').cycle({
			fx: 'scrollLeft',
			timeout: 11500,
			speed: 250,
			before: StepOne.textGalleryChange
 		});
	}, 
	textGalleryChange:function(){
		var _index = ($(this).index() + 1) === 7 ? 0 : ($(this).index() + 1);
		textGalleryTimeout = setTimeout(function(){
			triggerVideowall('showModel', _index);
		},8750);
	}
};

var StepTwo = {
	init:function(){
		$("#step_one_gallery").cycle('pause');
		clearTimeout(textGalleryTimeout);
		playerStatus = 1;
		
		triggerVideowall('showIntro', 0);
		
		SwitchContent.switch($("#step_two_a"));
		timerStepTwoB = setTimeout(function(){SwitchContent.switch($("#step_two_b"))},2500);
		timerStepTwoC = setTimeout(function(){SwitchContent.switch($("#step_two_c"))},5000);
		timerStepThree = setTimeout(function(){SwitchContent.switch($("#step_three")); StepThree.init(); },7500);
		Restart.global();
	}
};

var StepThree = {
	init:function(){
		this.attachEvents();
	},
	attachEvents:function(){
		$("#step_three a").click(function(){
			$(this).parent().addClass('active');
			timerStepFour = setTimeout(function(){ SwitchContent.switch($("#step_four")); StepFour.init(); },200);
			return false;
		});
	}
};

var StepFour = {
	init: function(){
		Restart.global();
		$("#step_three").removeClass('active');
		
		triggerVideowall('showSlide', 0);
		
		this.attachEvents();
	},
	attachEvents:function(){
		$("#gallery_text").cycle({
			fx: 'scrollLeft',
			timeout: 0, 
			speed: 'fast'
		});
		$("#buttons a").click(function(){
			Restart.global();
			$("#gallery_text").cycle($(this).index('#buttons a'));  
			$("#buttons a.active").removeClass('active');
			$("#step_four #header_text h1").text($(this).addClass('active').find('span').text());

			triggerVideowall('showSlide', $(this).index('#buttons a'));

			return false;
		});
	}
};

var Restart = {
	timeoutCounter: 5000,
	init:function(){
		timerRestart = setTimeout(function(){ Restart.restart() },Restart.timeoutCounter);
	},
	restart:function(){
		playerStatus = 0;
		// Kill all exisiting timers
		//clearTimeout(globalTimeout);
		clearTimeout(timerStepTwoB);
		clearTimeout(timerStepTwoC);
		clearTimeout(timerStepThree);
		clearTimeout(timerStepFour);
		//Reset Buttons
		$("#gallery_text").cycle(0);  
		$("#buttons a.active").removeClass('active');
		$("#step_four #header_text h1").text($("#buttons a").eq(0).addClass('active').find('span').text());
		
		SwitchContent.switch($("#step_one"));
		StepOne.textGallery();

		triggerVideowall('showThreeJS', 0);

		triggerVideowall('showModel', 0);

	},
	global:function(){
		//clearTimeout(globalTimeout);
		//globalTimeout = setTimeout(function(){ Restart.restart() }, 30000);
	}
}

function startPlayer() {
	clearTimeout(timerRestart);
	// Kill all exisiting timers
	if(!playerStatus){
		//clearTimeout(globalTimeout);
		clearTimeout(timerStepTwoB);
		clearTimeout(timerStepTwoC);
		clearTimeout(timerStepThree);
		clearTimeout(timerStepFour);
		StepTwo.init();
		window.location.hash = "changed"
	}
}
function restartPlayer() {
	Restart.init();
}

Restart.init();
